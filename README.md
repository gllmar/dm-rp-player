# dm-rp-player

Distributed memory raspberry pi video player

[TOC]

## install to fresh raspberry 

### install OS Raspberry pi OS lite

### enable ssh

```
touch /boot/ssh
```
### ssh in 

### update 

```
sudo apt-get update && sudo apt-get upgrade
```

### raspi-config

```
sudo raspi-config
```

* wireless lan contry
* password
* hostname -> dm-pi-x
* console autologin pi

```
sudo reboot
```

#### fix locales ?

https://gist.github.com/okaufmann/79acf73fd9f41b0b189cc3c8c480eaf5

```
echo "LC_ALL=en_US.UTF-8" | sudo tee -a /etc/environment > /dev/null
echo "en_US.UTF-8 UTF-8" | sudo tee -a /etc/locale.gen > /dev/null
echo "LANG=en_US.UTF-8" | sudo tee /etc/locale.conf > /dev/null
sudo locale-gen en_US.UTF-8
```

#### install apps

```
sudo apt-get install tmux git
```

##### micro
not avaible on debian buster

```
sudo apt-get install xclip xsel

curl https://getmic.ro | bash

sudo mv ./micro /usr/local/bin/

```

#### avahi-ssh
```
sudo su
curl https://raw.githubusercontent.com/lathiat/avahi/master/avahi-daemon/ssh.service > /etc/avahi/services/ssh.service
exit
sudo systemctl restart avahi-daemon.service 
```

#### samba 
##### install
```
sudo apt-get update 
sudo apt-get install samba samba-common-bin
sudo smbpasswd -a pi
sudo service smbd restart
```


##### rw home folder

```
micro /etc/samba/smb.conf 
```
~line 176
```diff
- read only = yes
+ read only = no
```

```bash
sudo systemctl restart smbd.service 
```

### manage keys

If new key 

#### [generate key](https://docs.gitlab.com/ee/ssh/index.html#generate-an-ssh-key-pair)
```
ssh-keygen -t rsa -b 2048 -C "dm-rp-player"
```

If importing key
####
```
mkdir ~.ssh
```
copy ```id_rsa id_rsa.pub``` to folder 

#### [ssh no password](https://www.thegeekstuff.com/2008/11/3-steps-to-perform-ssh-login-without-password-using-ssh-keygen-ssh-copy-id/)

from remote computer
```
ssh-copy-id -i ~/.ssh/id_ed25519.pub pi@dm-rp-x.local
```

### Sources

#### pi_hello_video

```bash
cd ~/src
git clone git@github.com:gllmAR/pi_hello_video.git

cd pi_hello_video

./rebuild.sh
cd hello_video
sudo make install

```

#### puredata 

```bash
cd ~/src
sudo apt-get install autoconf libtool libasound2-dev build-essential autoconf automake libtool gettext git libasound2-dev libjack-jackd2-dev libfftw3-3 libfftw3-dev tcl tk
git clone https://github.com/pure-data/pure-data
cd pure-data

./autogen.sh

./configure --enable-jack --enable-fftw

make -j4
sudo make install
```

#### dm-rp-player

```bash
cd ~/src 

git clone git@gitlab.com:gllmar/dm-rp-player.git

cd dm-rp-player 

service/install-dm-rp-player.sh 

```
##### sync media

ssh-copy-id -i ~/.ssh/id_ed25519.pub pi@dm-rp-x.local




#### Fix fan curve

https://www.jeffgeerling.com/blog/2021/taking-control-pi-poe-hats-overly-aggressive-fan

sudo micro /boot/config.txt

```
# PoE Hat Fan Speeds
dtparam=poe_fan_temp0=50000
dtparam=poe_fan_temp1=60000
dtparam=poe_fan_temp2=70000
dtparam=poe_fan_temp3=80000
```



### tweak fstab (optionnal)

```bash
sudo mkdir  /mnt/diaspora/
sudo mkdir  /mnt/diaspora/projets

sudo micro /etc/fstab
```

```
//nomade.local/projets  /mnt/nomade/projets  cifs  username=myname,password=123  0  0

```

### Copy media (optional)

```
mkdir ~/media && mkdir ~/media/dm

rsync -r --progress /mnt/nomade/projets/sabrina/distributed_memories/h264/ ~/media/dm

```
### Sync Media DB (opt)

```
scripts/sync_media_db.sh
```
